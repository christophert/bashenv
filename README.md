# BASH Environment Aliases and scripts

## Current Aliases
### General
* fuck = rerun with sudo
* prcd = change directory to Projects

### git
* ctclone = prcd and run ctclone script
* ga = git add <files>
* gs = git status
* gpu = git pull
* gp = git push
* gc = git commit -am "<message>"
* gsi = git submodule init && git submodule update

## Scripts
### git
* ctclone.sh = pull from bitbucket
